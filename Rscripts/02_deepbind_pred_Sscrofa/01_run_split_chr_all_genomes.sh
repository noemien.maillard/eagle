ref_seq=/media/nomaillard/Storage/EAGLE/data/ref_seq/
for org in `ls $ref_seq`;
do
	for genome in `ls $ref_seq$org/*/*.fa.gz`;
	do
		org_name=$(echo $genome | grep -Eo './\w+\.')
		org_name=${org_name:2:-1}
		org_dir="/media/nomaillard/Storage/EAGLE/data/deepbind_genome_binned/deepbind_"$org_name"_genome_binned/"
		mkdir -p $org_dir
		./01_split_chr_for_multiprocessing_withparser.R -g $genome --bed_path $org_dir
	done
done

