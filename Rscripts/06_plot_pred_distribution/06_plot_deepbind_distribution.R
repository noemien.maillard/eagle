# Creation date: 2024/02/22
# Last review: 2024/02/22
# By Noémien MAILLARD
# Laboratory: GENEPI
# Project: EAGLE
# Script aim: Plot deepbind peak predicted distribution.
# Modules versions: R version 4.3.2, ggbreak 0.1.2, ggplot2 3.4.4, rtracklayer 1.62.0


library(ggbreak)
library(ggplot2)


h.path <- "/media/nomaillard/Storage/EAGLE/plots_best_thresholds/deepbind/ENCODE_human_deepbind/files_from_deepbind_supp_table4/bed_files/"
h.files <- list.files(h.path)
h.files <- h.files[!h.files %in% c("CTCF.bed")]
h.files <- paste0(h.path, h.files)
h.files <- c(
  h.files,
  "/media/nomaillard/Storage/EAGLE/plots_best_thresholds/deepbind/ENCODE_human_deepbind/files_from_ENCODE/bed_files/CTCF_HepG2.bed"
  )

list_thresholds.obs <- lapply(
  h.files,
  function(x){
    bed.file <- sub(".*/bed_files/*", "", x)
    c(sub("*\\.bed", "", bed.file), length(rtracklayer::import.bed(x)))
  }
)
list_thresholds.obs[[21]][1] = "CTCF"

table_thresholds.obs <- data.table::rbindlist(lapply(list_thresholds.obs, as.list))
colnames(table_thresholds.obs) <- c("protein", "human.peaks")

db.path <- "/media/nomaillard/Storage/EAGLE/predictions_from_Sscrofa_refseq/deepbind/whole_genome/bed_cutoff/"
db.pred <- list.files(db.path)

list_thresholds.pred <- lapply(
  db.pred,
  function(x){
    bed.file <- sub(".*/bed_cutoff/*", "", x)
    c(sub("*\\.bed", "", bed.file), length(rtracklayer::import.bed(paste0(db.path, x))))
  }
)

table_thresholds.pred <- data.table::rbindlist(lapply(list_thresholds.pred, as.list))
colnames(table_thresholds.pred) <- c("protein", "pred.peaks")

table.thresholds <- merge(table_thresholds.obs, table_thresholds.pred, by='protein')
table.thresholds$human.peaks <- as.numeric(table.thresholds$human.peaks)
table.thresholds$pred.peaks <- as.numeric(table.thresholds$pred.peaks)
table.thresholds <- table.thresholds[order(protein),]

melted_table.thresholds <- reshape2::melt(table.thresholds, id.vars="protein")

fill_palette <- c(
  'pred.peaks' = "#bb2c2c",
  'human.peaks' = "#2c6bbb"
)
# few expe => 1 col by expe (pred vs human)
db.plot <- ggplot(data=melted_table.thresholds, aes(x=protein, y=value, group=variable, fill=variable)) +
  geom_col(width=0.5, position='dodge') +
  scale_fill_manual(name="variable", values=fill_palette) +
  scale_y_log10(breaks = scales::trans_breaks("log10", function(x) 10^x), labels = scales::trans_format("log10", scales::math_format(.x))) +
  theme_classic(base_size=15) +
  theme(axis.text.x=element_text(angle=45, hjust=1)) +
  ylab("log10(n.peaks)")
ggsave(
  filename="/media/nomaillard/Storage/EAGLE/results/deepbind/deepbind_chip_distri.png",
  plot=db.plot
)

# boxplot representation
db.boxplot <- ggplot(data=melted_table.thresholds, aes(x=variable, y=value, fill=variable)) +
  geom_boxplot(show.legend=F) +
  scale_fill_manual(name="variable", values=fill_palette) +
  scale_y_log10(breaks = scales::trans_breaks("log10", function(x) 10^x), labels = scales::trans_format("log10", scales::math_format(.x))) +
  theme_classic(base_size=18) +
  theme(
    axis.title.x=element_blank(),
    axis.text.x=element_text()
    ) +
  ylab("log10(n.peaks)")
ggsave(
  filename="/media/nomaillard/Storage/EAGLE/results/deepbind/deepbind_boxplot_chip_distri.png",
  plot=db.boxplot
)
