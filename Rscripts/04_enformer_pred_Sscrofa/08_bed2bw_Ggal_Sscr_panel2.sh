#!/usr/bin/bash


tissues=(fat lung muscle spleen)

# Ggal
for tissue in ${tissues[@]}
do
	./02_bed2bigwig_parser.R --bed_path /media/nomaillard/Storage/EAGLE/data/enformer_genome_binned/enformer_Gallus_gallus_genome_binned \
		--results_path /media/nomaillard/Storage/EAGLE/results/enformer/Ggallus/01_predictions/$tissue/concat_chrom/ \
		--save_path /media/nomaillard/Storage/EAGLE/results/enformer/Ggallus/02_bigwig/$tissue/
done

# Sscr
for tissue in ${tissues[@]}
do
	./02_bed2bigwig_parser.R --bed_path /media/nomaillard/Storage/EAGLE/data/enformer_genome_binned/enformer_Sus_scrofa_genome_binned \
		--results_path /media/nomaillard/Storage/EAGLE/results/enformer/Sscrofa/Sscrofa11.1/01_predictions/$tissue/concat_chrom/ \
		--save_path /media/nomaillard/Storage/EAGLE/results/enformer/Sscrofa/Sscrofa11.1/02_bigwig/$tissue/
done
