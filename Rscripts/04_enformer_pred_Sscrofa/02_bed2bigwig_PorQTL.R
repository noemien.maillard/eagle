# Creation date: 2024/01/26
# Last review: 2024/01/26
# By Noémien MAILLARD
# Laboratory: GENEPI
# Project: EAGLE
# Script aim: Generate bw files from enformer prediction data. Inspired from 02_bed2bigwig_lung_spleen (EAGLE).
# Comment: runtime ~1h30 (40,964 bw files), RAM ~45G
# Modules versions: R version 4.3.2, GenomicRanges 1.54.1, rtracklayer 1.62.0


library(dplyr)
library(GenomicRanges)


setwd('/media/nomaillard/Storage/EAGLE/')

# load bed files
bed.path <- "data/enformer_Sscrofa_genome_binned"
bed.files <- list.files(bed.path, pattern='*.bed', full.names=TRUE, recursive=TRUE)

# get file names without extension
file.names <- tools::file_path_sans_ext(
  list.files(
    "results/enformer/Sscrofa_genome_binned/blood/concat_chrom/",
    pattern='*.csv',
    recursive=T
  )  # close list.files
)  # close file_path_sans_ext

tissues <- c("blood/", "brain/", "fat/", "glands/", "muscle/", "pancreas/", "repro/")
patt2drop <- list(
  "blood/"=list("mononuclear", "stem"),
  "brain/"=list("smooth"),
  "fat/"=list("preadipo", "stem"),
  "glands/"=list(),
  "muscle/"=list("smooth", "colon", "duodenum"),
  "pancreas/"=list(),
  "repro/"=list("cancer")
  )

for (tissue in tissues){
  # load predictions (*.csv)
  enf.path <- paste0("results/enformer/Sscrofa_genome_binned/", tissue, "concat_chrom")
  enf.files <- list.files(enf.path, pattern='*.csv', full.names=TRUE, recursive=TRUE)
  
  save_path <- paste0("/media/nomaillard/Storage/EAGLE/results/enformer/Sscrofa_genome_binned/bigwig/", tissue)
  print(tissue)
  for (chrom in file.names){
    # get bed file(s) of given chromosome
    bed.file <- bed.files[which(lapply(
      file.names,
      function(x){grep(chrom, x)}
    )!=0)]
    bed.df <- read.csv(
      bed.file,
      header=F,
      sep='\t'
    )
    colnames(bed.df) <- c('chr', 'start', 'end')
    
    # get enformer file(s) of given chromosome
    enf.file <- enf.files[which(lapply(
      file.names,
      function(x){grep(chrom, x)}
    )!=0)]
    enf.df <- read.csv(
      enf.file,
      header=T,
      sep=','
    )
    print(length(enf.df))
    # drop experimental conditions of non-interest (patt2drop)
    if (length(patt2drop[[tissue]])>0){
      enf.df <- enf.df %>%
        select(!matches(unlist(patt2drop[[tissue]])))
    }
    # remove point at the end of experiment names
    names(enf.df) <- sub('\\.$', '', names(enf.df))
    print(length(enf.df))
    break
    
    # # setup bed GRanges
    # grnames <- Rle(bed.df$chr)
    # grpos <- IRanges(start=bed.df$start, end=bed.df$end)
    # lapply(
    #   colnames(enf.df),
    #   function(expe){
    #     scores <- enf.df[,expe]
    #     gr <- GRanges(
    #       seqnames=grnames,
    #       ranges=grpos,
    #       score=scores
    #     )
    #     # mandatory line for exporting
    #     seqlengths(gr) <- max(split(end(gr), seqnames(gr)))
    #     filename <- paste0(save_path, chrom, "_", expe, ".bw")
    #     # export to bigwig format
    #     rtracklayer::export.bw(object=gr, con=filename, format="bw")
    #   }
    # )
  }
}
