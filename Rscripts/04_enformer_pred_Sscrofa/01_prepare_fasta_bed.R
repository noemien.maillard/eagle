# Creation date: 2024/01/12
# Last review: 2022/01/12
# By Noémien MAILLARD
# Laboratory: GENEPI
# Project: EAGLE
# Script aim: Prepare Sscrofa genome (fasta and bed files) to run enformer.
# Modules versions: R version 4.3.2, readxl 1.4.3, GenomicRanges 1.54.1, BSgenome 1.70.1, BSgenome.Sscrofa.UCSC.susScr11 1.4.2, seqinr 4.2-36


setwd("/media/nomaillard/Storage/EAGLE/")


library(readxl)
library(GenomicRanges)
library(BSgenome.Sscrofa.UCSC.susScr11)
library(seqinr)

window=114688  # predicted window of enformer is 114688
receptive_field=393216  # receptive window of enformer
shift <- (receptive_field-window)/2  # shift to start receptive field from 1
max.win=500  # maximum windows per file

# slide ranges to start GRanges from 1 with ranges of receptive_field range
slide_ranges <- function(gr, shift){
  end(gr) <- end(gr)+shift
  start(gr) <- start(gr)+shift
  return(gr)
}

# break a GRanges into GRangesList with n ranges per GR
# gr = GRanges object, n = nb ranges per gr.
break_gr <- function(gr, n){
  n.gr <- trunc(length(gr)/n)
  gr.list <- vector(mode='list', length=n.gr+1)
  for (i in 1:n.gr){
    # calculate start/end range (rg)
    start.rg <- (i-1)*n+1
    end.rg <- i*n
    new_gr <- gr[c(start.rg:end.rg)]
    gr.list[[i]] <- new_gr
  }
  # same calculus as in the loop but i=n.gr
  start.rg <- (n.gr)*n+1
  gr.list[[n.gr+1]] <- gr[c(start.rg:length(gr))]
  
  return (gr.list)
}


# store seqnames and seqlengths for each chr/scaffold
df_seq <- data.frame(
  "seqname"=names(seqlengths(BSgenome.Sscrofa.UCSC.susScr11)),
  "seqlength"=seqlengths(BSgenome.Sscrofa.UCSC.susScr11)
)

for (chr in seqnames(BSgenome.Sscrofa.UCSC.susScr11)){
  chr.len <- df_seq$seqlength[df_seq$seqname==chr]
  # ignore chrom/scaffolds without enough length
  if (chr.len < receptive_field){next}
  # generate GRanges with chromosome/scaffold with windows of receptive_field length centered on window sized windows
  win.GR=GRanges(chr,IRanges(breakInChunks(totalsize=chr.len,chunksize=window)))
  win.GR=slide_ranges(win.GR, shift=shift)
  # keep only ranges lower than chr.len
  win.GR=win.GR[end(win.GR)<chr.len]
  win_resized.GR=resize(win.GR, width=receptive_field, fix="center")
  # keep only ranges lower than chr.len
  win_resized.GR=win_resized.GR[end(win_resized.GR)<chr.len]
  # keep as much 114kb windows as 393kb resized windows
  win.GR=win.GR[1:length(win_resized.GR)]
  # checkpoint: after sliding and resizing, GR should start at 1
  if (start(win_resized.GR)[1] != 1){
    print(chr)
    print("First range does not start at 1. Review code.")
    break
  }
  dir.create(paste0("data/enformer_Sscrofa_genome_binned/", chr))
  data_dir <- paste0("data/enformer_Sscrofa_genome_binned/", chr, "/")
  if (length(win_resized.GR)>max.win){
    gr.list <- break_gr(gr=win_resized.GR, n=max.win)
    for (i in 1:length(gr.list)){
      gr <- gr.list[[i]]
      gr.seq=DNAStringSet(getSeq(
        BSgenome.Sscrofa.UCSC.susScr11,
        names=seqnames(gr),
        start=start(gr),
        end=end(gr)
      ))
      names(gr.seq)=paste0("window",1:length(gr.seq))
      writeXStringSet(x=gr.seq, filepath=paste0(data_dir, chr, "_", i, ".enformer_windows_393216b.fa"))
    }
  }else{
    # get seq according to set windows and rename each window with numbers to generate 1 file.fa with n numbered windows
    win_resized.seq=DNAStringSet(getSeq(
      BSgenome.Sscrofa.UCSC.susScr11,
      names=seqnames(win_resized.GR),
      start=start(win_resized.GR),
      end=end(win_resized.GR)
      ))
    names(win_resized.seq)=paste0("window",1:length(win_resized.seq))
    writeXStringSet(x=win_resized.seq,filepath=paste0(data_dir, chr, ".enformer_windows_393216b.fa"))
  }
  
  # bin GR into 128bp bins and write bed of bins
  bin.GR=GRanges(chr,IRanges(breakInChunks(totalsize=end(win.GR[length(win.GR)]),chunksize=128)))
  ol=findOverlaps(bin.GR,win.GR)
  bin.GR=bin.GR[queryHits(ol)]
  
  write.table(as.data.frame(bin.GR)[,1:3],file=paste0(data_dir, chr, ".enformer_bins_128b.bed"),
              row.names=F,col.names=F,sep='\t',quote=F)
}
