#!/usr/bin/bash


# species dna
pred_path=/media/nomaillard/Storage/EAGLE/results/enformer/Ggallus/02_bigwig
output_path=/media/nomaillard/Storage/EAGLE/results/enformer/auc_enformer/01_panel2/Ggallus

# proteins and observations
proteins=(CTCF H3K4me1 H3K4me3 H3K27ac H3K27me3)
Ggallus_obs_path=/media/nomaillard/Storage/EAGLE/data/Ggallus/bed_ncbi_geo

declare -A t_dict
t_dict=(["adipose"]="adipose fat" ["liver"]="liver hepg2" ["lung"]="lung" ["muscle"]="muscle myo" ["spleen"]="spleen")

for protein in ${proteins[@]}
do
	for tissue in ${!t_dict[@]}
	do
		if [[ $tissue = "adipose" ]] || [[ $tissue = "liver" ]]
		then
			./09_auROC_auPRC_parser.R \
				--pred_path $pred_path/fat/ \
				--protein $protein \
				--tissues ${t_dict[$tissue]} \
				--obs_path $Ggallus_obs_path \
				--fread_obs \
				--output $output_path/$protein$tissue"_Ggallus.tsv" \
				--log $output_path/$protein$tissue"_Ggallus.log"
		else
			continue
			./09_auROC_auPRC_parser.R \
				--pred_path $pred_path/$tissue/ \
				--protein $protein \
				--tissues ${t_dict[$tissue]} \
				--obs_path $Ggallus_obs_path \
				--fread_obs \
				--output $output_path/$protein$tissue"_Ggallus.tsv" \
				--log $output_path/$protein$tissue"_Ggallus.log"
		fi
	done
done
