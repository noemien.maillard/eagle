#!/usr/bin/bash


# species dna
output=/media/nomaillard/Storage/EAGLE/results/deepbind/auc_deepbind/03_panel3/Sscrofa
bins=$(ls /media/nomaillard/Storage/EAGLE/data/deepbind_genome_binned/deepbind_Sus_scrofa_genome_binned/*/*.bed)
pred_path=/media/nomaillard/Storage/EAGLE/results/deepbind/Sscrofa/Sscrofa11.1/02_bigwig
repeats=/media/nomaillard/Storage/EAGLE/data/Sscrofa/RepeatMasker/rmsk.txt.gz

# proteins and observations
proteins=(CTCF)
obs_path=/media/nomaillard/Storage/EAGLE/data/Sscrofa/bed_ncbi_geo

for protein in ${proteins[@]}
do
	./01_auROC_auPRC_parser.R \
		--pred_path $pred_path \
		--protein $protein \
		--obs_path $obs_path \
		--output $output/$protein.tsv \
		--fread_obs \
		--repeat_table $repeats \
		--log $output/$protein.log
done
