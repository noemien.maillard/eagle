# Creation date: 2024/03/11
# Last review: 2024/03/11
# By Noémien MAILLARD
# Laboratory: GENEPI
# Project: EAGLE
# Script aim: Cut QTL1 region to regenerate haplotypes with FaLcOn.
# Modules versions: R version 4.3.3, Biostrings 2.70.2, BSgenome 1.70.2, BSgenome.Sscrofa.UCSC.susScr11 1.4.2, GenomicRanges 1.54.1, IRanges 2.36.0


setwd("/media/nomaillard/Storage/EAGLE/")


# extract 1Mb GRanges centered on QTL1 (Sscrofa), given it a name and save in fasta file
gr <- GenomicRanges::GRanges(
  'chr1',
  IRanges::IRanges(
    start=270380000,
    end=270680000
  )
)
gr <- GenomicRanges::resize(gr, width=1000001, fix="center")
gr.seq <- Biostrings::DNAStringSet(BSgenome::getSeq(
  BSgenome.Sscrofa.UCSC.susScr11::BSgenome.Sscrofa.UCSC.susScr11,
  names=GenomicRanges::seqnames(gr),
  start=GenomicRanges::start(gr),
  end=GenomicRanges::end(gr)
))
names(gr.seq)=paste0(
  "ref|R BSgenome.Sscrofa.UCSC.susScr11.1|chr1:",
  as.character(GenomicRanges::start(gr)[1]),
  "-",
  as.character(GenomicRanges::end(gr)[1])
  )

Biostrings::writeXStringSet(x=gr.seq, filepath="data/enformer_impact_scores/Sscrofa11.1Mb_QTL1centered.fa")
