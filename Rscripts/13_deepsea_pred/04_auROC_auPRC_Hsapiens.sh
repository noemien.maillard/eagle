#!/usr/bin/bash


# species dna
pred_path=/media/nomaillard/Storage/EAGLE/results/deepsea/Hsapiens/02_bigwig
output_path=/media/nomaillard/Storage/EAGLE/results/deepsea/auc_deepsea/Hsapiens

# proteins and observations
proteins=(MAX MYC NANOG YY1 SIN3A RBBP5 POU5F1 SUZ12 CTCF dnase H3K4me1 H3K4me3 H3K27ac H3K27me3)
ft=(MAX MYC NANOG YY1 SIN3A RBBP5 POU5F1 SUZ12 CTCF)
Hsapiens_obs_path=/media/nomaillard/Storage/EAGLE/data/Hsapiens/deepsea_training/02_liftover

for protein in ${proteins[@]}
do

	if [[ " ${ft[*]} " = *"$protein"* ]]; then
		# pred vs human obs (TF)
		./04_auROC_auPRC_parser.R \
			--pred_path $pred_path \
			--protein $protein \
			--obs_path $Hsapiens_obs_path/transcription_factors \
			--fread_obs \
			--output $output_path/$protein"_Hsapiens.tsv" \
			--lifted \
			--log $output_path/$protein"_Hsapiens.log"
	else
		# pred vs human obs (histone marks and DNase)
		./04_auROC_auPRC_parser.R \
			--pred_path $pred_path \
			--protein $protein \
			--obs_path $Hsapiens_obs_path/histone_marks_dnase \
			--fread_obs \
			--output $output_path/$protein"_Hsapiens.tsv" \
			--lifted \
			--log $output_path/$protein"_Hsapiens.log"
	fi
done
