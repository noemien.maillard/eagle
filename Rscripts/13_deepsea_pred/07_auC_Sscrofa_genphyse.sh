#!/usr/bin/bash


# species dna
pred_path=/media/nomaillard/Storage/EAGLE/results/deepsea/Sscrofa/
output_path=/media/nomaillard/Storage/EAGLE/results/deepsea/auc_deepsea/04_panel4/
breeds=(Sscrofa_AB63083986_LW_ref Sscrofa_AB75005564_LWm3 Sscrofa_AB75005811_DU Sscrofa_AB75069227_PE810 Sscrofa_AB75069229_LRm6 Sscrofa_AB75132735_LW Sscrofa_AB75167259_PI Sscrofa_AB75230671_LR Sscrofa_AB75230687_TZM)

# proteins and observations
proteins=(CTCF H3K4me1 H3K4me3 H3K27ac H3K27me3)
Sscrofa_obs_path=/media/nomaillard/Storage/EAGLE/data/Sscrofa/bed_ncbi_geo

declare -A t_dict
t_dict=(["liver"]="liver hepg2" ["lung"]="lung a549" ["muscle"]="muscle hsmm")

for breed in ${breeds[@]}
do
	breed_path=$pred_path$breed'/02_bigwig/'
	output_breed=$output_path$breed
	for protein in ${proteins[@]}
	do
		for tissue in ${!t_dict[@]}
		do
			./04_auROC_auPRC_parser.R \
				--pred_path $breed_path \
				--protein $protein \
				--tissues ${t_dict[$tissue]} \
				--obs_path $Sscrofa_obs_path \
				--fread_obs \
				--output $output_breed/$protein$tissue"_"$breed".tsv" \
				--log $output_breed/$protein$tissue"_"$breed".log"
		done
	done
done
