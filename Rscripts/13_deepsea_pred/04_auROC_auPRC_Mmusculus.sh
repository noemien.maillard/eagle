#!/usr/bin/bash


# species dna
pred_path=/media/nomaillard/Storage/EAGLE/results/deepsea/Mmusculus/02_bigwig
output_path=/media/nomaillard/Storage/EAGLE/results/deepsea/auc_deepsea/Mmusculus

# proteins and observations
proteins=(MAX MYC NANOG YY1 SIN3A RBBP5 POU5F1 SUZ12 CTCF dnase H3K4me1 H3K4me3 H3K27ac H3K27me3)
ft=(MAX MYC NANOG YY1 SIN3A RBBP5 POU5F1 SUZ12 CTCF)
Mmusculus_obs_path=/media/nomaillard/Storage/EAGLE/data/Mmusculus/bed/02_liftover

for protein in ${proteins[@]}
do

	if [[ " ${ft[*]} " = *"$protein"* ]]; then
		# pred vs mouse obs (TF)
		./04_auROC_auPRC_parser.R \
			--pred_path $pred_path \
			--protein $protein \
			--obs_path $Mmusculus_obs_path/remap_ft \
			--fread_obs \
			--output $output_path/$protein"_Mmusculus.tsv" \
			--lifted \
			--log $output_path/$protein"_Mmusculus.log"
	else
		# pred vs mouse obs (histone marks and DNase)
		./04_auROC_auPRC_parser.R \
			--pred_path $pred_path \
			--protein $protein \
			--obs_path $Mmusculus_obs_path/encode_histones \
			--fread_obs \
			--output $output_path/$protein"_Mmusculus.tsv" \
			--lifted \
			--log $output_path/$protein"_Mmusculus.log"
	fi
done
