ref_seq=/media/nomaillard/Storage/EAGLE/data/ref_seq/
for org in `ls $ref_seq`;
do
	if [[ $org == "Mmusculus" ]]
	then
		continue
	fi

	for genome in `ls $ref_seq$org/*/*.fa.gz`;
	do
		org_name=$(echo $genome | grep -Eo './\w+\.')
		org_name=${org_name:2:-1}
		org_dir="/media/nomaillard/Storage/EAGLE/data/deepsea_genome_binned/deepsea_"$org_name"_genome_binned/"
		mkdir -p $org_dir
		./02_split_chr_parser.R -g $genome --bed_path $org_dir -n 10 --log $org_dir$org_name.log
	done
done

