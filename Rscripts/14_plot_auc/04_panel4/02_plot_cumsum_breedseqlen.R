# Creation date: 2024/10/02
# Last review: 2024/10/02
# By Noémien MAILLARD
# Laboratory: GENEPI
# Project: EAGLE
# Script aim: Plot cumsum of scaffold lengths for each breed.
# Modules versions: R version 4.4.0, Biostrings 2.72.1, dplyr 1.1.4, ggplot2 3.5.1


library(Biostrings)
library(dplyr)
library(ggplot2)


list.fasta <- list.files('/media/nomaillard/Storage/EAGLE/data/ref_seq/Sscrofa/Sscrofa_allbreeds_release112/', pattern='.fa.gz', full.names=T)
breeds <- list.files('/media/nomaillard/Storage/EAGLE/data/ref_seq/Sscrofa/Sscrofa_allbreeds_release112/', pattern='.fa.gz')
breeds <- sub('\\.dna.*', '', breeds)
breeds <- sub('Sus_scrofa[_\\.]', '', breeds)
breeds <- sub('*\\.[a-zA-Z].*', '', breeds)
dt.seq <- data.table::rbindlist(lapply(
  breeds,
  function(br){
    seq <- readDNAStringSet(list.fasta[grep(br, list.fasta)])
    
    data.table::data.table(
      "breed"=br,
      "seqname"=names(seq),
      "seqlength"=seqlengths(seq)
    )
  }
))


for (br in unique(dt.seq$breed)){
  print(br)
  sumseq <- sum(dt.seq$seqlength[dt.seq$breed==br])
  plt <- filter(dt.seq, breed==br) %>%
      ggplot(aes(x=reorder(seqname, cumsum(as.numeric(seqlength))), y=cumsum(as.numeric(seqlength)))) +
      geom_col() +
      geom_hline(yintercept=0.9*sumseq) +
      geom_hline(yintercept=0.95*sumseq) +
      geom_hline(yintercept=0.99*sumseq) +
      scale_y_log10(breaks = scales::trans_breaks("log10", function(x) 10^x, n=2),
                    labels = scales::trans_format("log10", scales::math_format(10^.x))) +
      scale_y_continuous(sec.axis=sec_axis(~./sumseq, labels=scales::label_percent())) +
      theme(
        legend.position="none",
        axis.text.x=element_blank(),
        axis.ticks.x=element_blank(),
        axis.line=element_line(linewidth=0.5)
      )
  ggsave(paste0("/media/nomaillard/Storage/EAGLE/results/plots_panel4/ensembl_scaffolds/cumsum/", br, ".png"), unit="cm", width=17)
}
