#!/usr/bin/bash


deepbind_tf_down="/media/nomaillard/Storage/EAGLE/data/Hsapiens/deepbind_training/01_downloaded"
deepbind_tf_lifted="/media/nomaillard/Storage/EAGLE/data/Hsapiens/deepbind_training/02_liftover"
deepsea_tf_down="/media/nomaillard/Storage/EAGLE/data/Hsapiens/deepsea_training/01_downloaded/transcription_factors"
deepsea_tf_lifted="/media/nomaillard/Storage/EAGLE/data/Hsapiens/deepsea_training/02_liftover/transcription_factors"
hist_down="/media/nomaillard/Storage/EAGLE/data/Hsapiens/deepsea_training/01_downloaded/histone_marks_dnase"
hist_lifted="/media/nomaillard/Storage/EAGLE/data/Hsapiens/deepsea_training/02_liftover/histone_marks_dnase"
chain="/media/nomaillard/Storage/EAGLE/data/chain_over/hg19ToHg38.over.chain.gz"
liftOver="/home/nomaillard/Programs/liftOver"
tmp_dir="/media/nomaillard/Storage/EAGLE/data/Hsapiens/tmp/"

mkdir $tmp_dir

# deepbind transcription factors
# note: deepbind are .txt files and chr, start, end cols are 2-4 cols
for bed in `ls $deepbind_tf_down/*.txt.gz`
do
	if [[ bed == "CTCF_remap.bed.gz" ]]
	then
		zcat $bed | cut -f 1-3 > $deepbind_tf_lifted"/CTCF_remap_lifted.bed"
		continue
	fi
	cut_bed=$(basename $(echo $bed | sed 's/.txt.gz/_cut.bed/'))
	if [[ $cut_bed == "CTCF" ]]
	then
		zcat $bed | cut -f 1-3 > $tmp_dir$cut_bed
	else
		zcat $bed | cut -f 2-4 > $tmp_dir$cut_bed
	fi
	new_bed=$(basename $(echo $bed | sed 's/.txt.gz/_lifted.bed/'))
	unmapped=$(basename $(echo $bed | sed 's/.txt.gz/_unMapped.bed/'))
	$liftOver $tmp_dir$cut_bed $chain $deepbind_tf_lifted/$new_bed $deepbind_tf_lifted/$unmapped
done

# deepSEA transcription factors
for bed in `ls $deepsea_tf_down/*.bed.gz`
do
	cut_bed=$(basename $(echo $bed | sed 's/.bed.gz/_cut.bed/'))
	zcat $bed | cut -f 1-3 > $tmp_dir$cut_bed
	new_bed=$(basename $(echo $bed | sed 's/.bed.gz/_lifted.bed/'))
	unmapped=$(basename $(echo $bed | sed 's/.bed.gz/_unMapped.bed/'))
	$liftOver $tmp_dir$cut_bed $chain $deepsea_tf_lifted/$new_bed $deepsea_tf_lifted/$unmapped
done

# histone marks
for bed in `ls $hist_down/*.bed.gz`
do
	cut_bed=$(basename $(echo $bed | sed 's/.bed.gz/_cut.bed/'))
	zcat $bed | cut -f 1-3 > $tmp_dir$cut_bed
	new_bed=$(basename $(echo $bed | sed 's/.bed.gz/_lifted.bed/'))
	unmapped=$(basename $(echo $bed | sed 's/.bed.gz/_unMapped.bed/'))
	$liftOver $tmp_dir$cut_bed $chain $hist_lifted/$new_bed $hist_lifted/$unmapped
done

rm -r $tmp_dir
