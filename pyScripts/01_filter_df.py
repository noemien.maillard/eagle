#! /usr/bin/python3
# Creation date: 2024/08/20
# Last review: 2024/08/20
# By Noémien MAILLARD
# Laboratory: GENEPI
# Project: EAGLE
# Script aim: Filter dataframe.

import argparse
import pandas as pd


def get_options():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-f',
        '--files',
        nargs='+',
      	help='List of files to filter.'
    )
    parser.add_argument(
        '--keep',
        default=None,
        nargs='+',
        help='Columns to keep. List of filters (patterns). \
            Please note FIRST filters conditions to keep THEN drop conditions to drop.'
    )
    parser.add_argument(
        '--drop',
        default=None,
        nargs='+',
        help='Columns to drop. List of filters (patterns). \
            Please note FIRST filters conditions to keep THEN drop conditions to drop.'
    )
    parser.add_argument(
        '--sep',
        default='\t',
        help='Input and output tables separator. Default to tab.'
    )
    parser.add_argument(
        '--from-hdf',
        action='store_true',
        help='If True, read hdf5 file instead of csv/tsv. Default to False.'
    )
    return parser.parse_args()


def filter_table(df, filters_keep_list, filters_drop_list):
    list_filtered = []
    for condition in filters_keep_list:
        list_filtered.append(df.loc[:,df.columns.str.contains(condition, case=False)])
    
    if not list_filtered == []:
        df = pd.concat(list_filtered, axis=1)
        df = df.loc[:,~df.columns.duplicated()].copy()

    for condition in filters_drop_list:
        df = df.loc[:,~df.columns.str.contains(condition, case=False)]
    
    return df


def loop_on_table(files_list, sep, from_hdf, keep_list, drop_list):
    for file in files_list:
        print(f'Filtering {file}')
        if from_hdf:
            df = pd.read_hdf(file)
        else:
            df = pd.read_csv(file, sep=sep)  # default engine for rapidity
        df = filter_table(df, filters_keep_list=keep_list, filters_drop_list=drop_list)
        df.to_csv(file, sep=args.sep, index=False)


if __name__ == '__main__':
    args = get_options()
    keep = args.keep if args.keep is not None else []
    drop = args.drop if args.drop is not None else []
    loop_on_table(
        files_list=args.files,
        sep=args.sep,
        from_hdf=args.from_hdf,
        keep_list=keep,
        drop_list=drop
    )
