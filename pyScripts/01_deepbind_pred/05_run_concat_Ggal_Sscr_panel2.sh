#!/usr/bin/bash

## ChIP Ggallus
# load directory containing 1 directory per scaffold and output
chr_dirs='/media/nomaillard/Storage/EAGLE/results/deepbind/Ggallus/01_predictions/'
output_path='/media/nomaillard/Storage/EAGLE/results/deepbind/Ggallus/01_predictions/whole_genome/'

mkdir -p $output_path

for scaffold in `ls $chr_dirs`;
do
	if [[ $scaffold == 'whole_genome' ]]; then continue; fi
	echo "concatenating files for $scaffold..."
	# scores_files are files containing predictions
	scores_files=`ls $chr_dirs$scaffold/*.txt`
	python concat_deepbind.py --files $scores_files --output $output_path$scaffold.bin.deepbind.txt
done


## ChIP Sscrofa
# load directory containing 1 directory per scaffold and output
chr_dirs='/media/nomaillard/Storage/EAGLE/results/deepbind/Sscrofa/Sscrofa11.1/01_predictions/'
output_path='/media/nomaillard/Storage/EAGLE/results/deepbind/Sscrofa/Sscrofa11.1/01_predictions/whole_genome/'

mkdir -p $output_path

for scaffold in `ls $chr_dirs`;
do
	if [[ $scaffold == 'whole_genome' ]]; then continue; fi
	echo "concatenating files for $scaffold..."
	# scores_files are files containing predictions
	scores_files=`ls $chr_dirs$scaffold/*.txt`
	python concat_deepbind.py --files $scores_files --output $output_path$scaffold.bin.deepbind.txt
done
