#!/usr/bin/bash


# ## ChIP hepG2
# # load deepbind, parameters selected with Rscript (02_select_deepbind_experiement) and directory containing 1 directory per scaffold
# deepbind='/home/nomaillard/Documents/Collab_GenEpi/programs/deepbind/deepbind'
# params='/home/nomaillard/Documents/Collab_GenEpi/programs/deepbind/db/samples/ChIP_hepG2'
# chr_dirs='/home/nomaillard/Documents/Collab_GenEpi/data/Sscrofa_genome_binned/ChIP_hepG2/'

## ChIP CTCF
# load deepbind, parameters selected with Rscript (02_select_deepbind_experiement) and directory containing 1 directory per scaffold
deepbind='/home/nomaillard/Documents/Collab_GenEpi/programs/deepbind/deepbind'
params='/home/nomaillard/Documents/Collab_GenEpi/programs/deepbind/db/samples/ctcf'
chr_dirs='/home/nomaillard/Documents/Collab_GenEpi/data/Sscrofa_genome_binned/ctcf/'

for scaffold in `ls $chr_dirs`
do
	echo "calculating scaffold $scaffold..."
	# load binned fasta
	list_binned_fasta=`ls $chr_dirs$scaffold/*.fa`
	# store outputs with same name as binned fasta but with txt extension
	outputs=$(for file in $list_binned_fasta; do echo ${file%.*}".txt";done)

	# run deepbind
	python deepbind_useCPU.py --deepbind $deepbind --params $params --binned-fasta $list_binned_fasta --outputs $outputs -p 20
done

