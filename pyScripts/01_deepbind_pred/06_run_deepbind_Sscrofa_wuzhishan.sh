#!/usr/bin/bash


# load deepbind parameters
deepbind='/home/nomaillard/Documents/Collab_GenEpi/programs/deepbind/deepbind'
params='/home/nomaillard/Documents/Collab_GenEpi/programs/deepbind/db/samples/ctcf/'
sscrofa='/media/nomaillard/Storage/EAGLE/data/deepbind_genome_binned/deepbind_Sus_scrofa_wuzhishan_genome_binned/'

for scaffold in `ls $sscrofa`
do
	echo "calculating scaffold $scaffold..."
	# load binned fasta
	list_binned_fasta=`ls $sscrofa$scaffold/*.fa`
	# store outputs with same name as binned fasta but with txt extension
	outputs=$(for file in $list_binned_fasta; do echo ${file%.*}".txt";done)

	# run deepbind
	python deepbind_useCPU.py --deepbind $deepbind --params $params --binned-fasta $list_binned_fasta --outputs $outputs -p 17 --log $sscrofa$scaffold/$scaffold.log
done

