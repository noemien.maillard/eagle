#!/usr/bin/bash

# fat (liver and adipose)
python3 01_run_enformer.py --fasta-path /media/nomaillard/Storage/EAGLE/data/enformer_genome_binned/enformer_Gallus_gallus_genome_binned/ \
	--filters-keep /media/nomaillard/Storage/EAGLE/results/enformer/Sscrofa/BSgenome/filter_files/fat.txt \
	--filters-drop fetal embryo crispr fibrobl endothelial cell_line origin derived \
	--results-path /media/nomaillard/Storage/EAGLE/results/enformer/Ggallus/01_predictions/fat/ \
	--log /media/nomaillard/Storage/EAGLE/results/enformer/Ggallus/01_predictions/fat/fat.log
# muscle
python3 01_run_enformer.py --fasta-path /media/nomaillard/Storage/EAGLE/data/enformer_genome_binned/enformer_Gallus_gallus_genome_binned/ \
	--filters-keep /media/nomaillard/Storage/EAGLE/results/enformer/Sscrofa/BSgenome/filter_files/muscle.txt \
	--filters-drop fetal embryo crispr fibrobl endothelial cell_line origin derived \
	--results-path /media/nomaillard/Storage/EAGLE/results/enformer/Ggallus/01_predictions/muscle/ \
	--log /media/nomaillard/Storage/EAGLE/results/enformer/Ggallus/01_predictions/muscle/muscle.log
# lung
python3 01_run_enformer.py --fasta-path /media/nomaillard/Storage/EAGLE/data/enformer_genome_binned/enformer_Gallus_gallus_genome_binned/ \
	--filters-keep lung \
	--filters-drop fetal embryo crispr fibrobl endothelial cell_line origin derived \
	--results-path /media/nomaillard/Storage/EAGLE/results/enformer/Ggallus/01_predictions/lung/ \
	--log /media/nomaillard/Storage/EAGLE/results/enformer/Ggallus/01_predictions/lung/lung.log
# spleen
python3 01_run_enformer.py --fasta-path /media/nomaillard/Storage/EAGLE/data/enformer_genome_binned/enformer_Gallus_gallus_genome_binned/ \
	--filters-keep spleen \
	--filters-drop fetal embryo crispr fibrobl endothelial cell_line origin derived \
	--results-path /media/nomaillard/Storage/EAGLE/results/enformer/Ggallus/01_predictions/spleen/ \
	--log /media/nomaillard/Storage/EAGLE/results/enformer/Ggallus/01_predictions/spleen/spleen.log
