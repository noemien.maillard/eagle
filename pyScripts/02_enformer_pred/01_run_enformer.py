#! /usr/bin/python3
# Creation date: 2024/01/12
# Last review: 2024/01/12
# By Noémien MAILLARD
# Laboratory: GENEPI
# Project: EAGLE
# Script aim: Run enformer on big data (like whole Sscrofa genome). Script adapted from jupyter notebook


# import enformer related libraries
import tensorflow as tf

# Make sure the GPU is enabled 
assert tf.config.list_physical_devices('GPU'), 'Start the colab kernel with GPU: Runtime -> Change runtime type -> GPU'
gpu_list = tf.config.list_physical_devices('GPU')
print(f"Num GPUs Available: {len(gpu_list)}, name(s): {gpu_list}.")

# Limit GPU memory usage
gpus = tf.config.experimental.list_physical_devices('GPU')
for gpu in gpus:
    tf.config.experimental.set_memory_growth(gpu, True)

import tensorflow_hub as hub
import kipoiseq
import pyfaidx
import pandas as pd
import numpy as np

# import utils
import argparse
from fnmatch import fnmatch
import logging
import os
from pathlib import Path
from shutil import move
import sys
from time import time


def get_options():
    parser = argparse.ArgumentParser()
    parser.add_argument(
       '--fasta-path',
       help='Path containing fasta files.'
    )
    parser.add_argument(
       '--filters-keep',
       default=None,
       nargs='+',
       help='Columns to keep. Can be a list of filters OR a file containing 1 condition to keep per line (case insensitive). eol="\\n"\n \
        Please note first filters conditions to keep then drop conditions to drop.'
    )
    parser.add_argument(
       '--filters-drop',
       default=None,
       nargs='+',
       help='Columns to drop. Can be a list of filters OR a file containing 1 condition to drop per line (case insensitive). eol="\\n"\n \
        Please note first filters conditions to keep then drop conditions to drop.'
    )
    parser.add_argument(
       '--results-path',
       help='Path to move results (csv).'
    )
    parser.add_argument(
        '--seq-len',
        default=393_216,
        help='Sequences length. Default to 393216bp.'
    )
    parser.add_argument(
       '--model-url',
       default='https://tfhub.dev/deepmind/enformer/1',
       help='Model url. Default to "https://tfhub.dev/deepmind/enformer/1".'
    )
    parser.add_argument(
       '--targets-url',
       default='https://raw.githubusercontent.com/calico/basenji/0.5/manuscripts/cross2020/targets_human.txt',
       help='URL to txt file containing target names. Default to https://raw.githubusercontent.com/calico/basenji/0.5/manuscripts/cross2020/targets_human.txt'
    )
    parser.add_argument(
        '--to-hdf',
        action='store_true',
        help='If True, save to hdf file format. Key will be "df1". Default to False.'
    )
    parser.add_argument(
        '--to-float16',
        action='store_true',
        help="If True, tables are saved in float16 (recommanded for big tables). Default to False (float32)."
    )
    parser.add_argument(
        '--log',
        help='Log file.'
    )

    variants = parser.add_argument_group('Score variants options')
    variants.add_argument(
        '--score-variants',
        action="store_true",
        help='If True, calculate impact scores instead of '
    )
    variants.add_argument(
        '--ref',
        default=None,
        help='Pattern to find in reference fasta filenames (may include other precisions like "snp", "ins", "dels"...).'
    )
    variants.add_argument(
        '--alt',
        default=None,
        help='Pattern to find in alternative fasta filenames (may include other precisions like "snp", "ins", "dels"...).'
    )
    variants.add_argument(
        '--name-output-scores',
        help='Output file name without extension (csv format).'
    )
    return parser.parse_args()
    

def get_files(path, ext):
    list_files = []
    for root, _, files in os.walk(path):
        for file in files:
            if file.endswith(ext):
                # os.path.abspath seems not mandatory but lock that we have absolute paths
                list_files.append(os.path.abspath(os.path.join(root, file)))

    return list_files


def get_files_vars(path, ref, alt):
    list_ref = []
    list_alt =[]
    files_root = ''
    for root, _, files in os.walk(path):
        for file in files:
            if fnmatch(file, f'*{ref}*.fa'):
                list_ref.append(os.path.abspath(os.path.join(root, file)))
            elif fnmatch(file, f'*{alt}*.fa'):
                list_alt.append(os.path.abspath(os.path.join(root, file)))
        if files_root != root:
            files_root=root

    return list_ref, list_alt, files_root


def one_hot_encode(sequence):
  return kipoiseq.transforms.F.one_hot(sequence, neutral_value=0).astype(np.float32)


def pred_from_fasta(fasta_file, seq_len):
    pyFa=pyfaidx.Fasta(fasta_file)
    pyFaNames=list(pyFa.keys())

    predictions=[]
    for j in range(len(pyFaNames)):
        sequence_one_hot_j = one_hot_encode(str(pyFa[pyFaNames[j]][0:seq_len])) # 393216 = 128 x 3072 bins
        predictions_j = model.predict_on_batch(sequence_one_hot_j[np.newaxis])['human'][0]
        predictions_j = tf.cast(predictions_j,tf.float16)
        predictions.append(predictions_j)
        del predictions_j

    predictions=np.concatenate(predictions)

    return predictions


def score_variants(path_fasta_ref, path_fasta_alt, seq_len):
    pyFaRef=pyfaidx.Fasta(path_fasta_ref)
    pyFaAlt=pyfaidx.Fasta(path_fasta_alt)
    pyFaNamesRef=list(pyFaRef.keys())
    pyFaNamesAlt=list(pyFaAlt.keys())

    predictions=[]
    for j in range(len(pyFaNamesRef)):
        ref_one_hot_j = one_hot_encode(str(pyFaRef[pyFaNamesRef[j]][0:seq_len]))
        alt_one_hot_j = one_hot_encode(str(pyFaAlt[pyFaNamesAlt[j]][0:seq_len]))
        predictions_j = model.predict_on_batch({
            'ref': ref_one_hot_j,
            'alt': alt_one_hot_j
            })
        predictions_j = tf.cast(predictions_j, tf.float16)
        predictions.append(predictions_j)
        
        del predictions_j

    return np.vstack(predictions)


def read_targets(targets_url):
    df_targets = pd.read_csv(targets_url, sep='\t')

    converter = lambda x: x.replace(' ', '_')
    var_names = list(map(converter, df_targets['description'].values.tolist()))

    return var_names


def filter_table(df, filters_keep_list, filters_drop_list):
    list_filtered = []
    for condition in filters_keep_list:
        list_filtered.append(df.loc[:,df.columns.str.contains(condition, case=False)])
    
    if list_filtered != []:
        kept_df = pd.concat(list_filtered, axis=1)
    else:
        kept_df = df
    for condition in filters_drop_list:
       kept_df = kept_df.loc[:,~kept_df.columns.str.contains(condition, case=False)]
    
    return kept_df


class Enformer:

  def __init__(self, tfhub_url):
    self._model = hub.load(tfhub_url).model

  def predict_on_batch(self, inputs):
    predictions = self._model.predict_on_batch(inputs)
    return {k: v.numpy() for k, v in predictions.items()}

  @tf.function
  def contribution_input_grad(self, input_sequence,
                              target_mask, output_head='human'):
    input_sequence = input_sequence[tf.newaxis]

    target_mask_mass = tf.reduce_sum(target_mask)
    with tf.GradientTape() as tape:
      tape.watch(input_sequence)
      prediction = tf.reduce_sum(
          target_mask[tf.newaxis] *
          self._model.predict_on_batch(input_sequence)[output_head]) / target_mask_mass

    input_grad = tape.gradient(prediction, input_sequence) * input_sequence
    input_grad = tf.squeeze(input_grad, axis=0)
    return tf.reduce_sum(input_grad, axis=-1)


class EnformerScoreVariantsRaw:

  def __init__(self, tfhub_url, organism='human'):
    self._model = Enformer(tfhub_url)
    self._organism = organism
  
  def predict_on_batch(self, inputs):
    ref_prediction = self._model.predict_on_batch(inputs['ref'][np.newaxis])[self._organism][0]
    alt_prediction = self._model.predict_on_batch(inputs['alt'][np.newaxis])[self._organism][0]

    return alt_prediction.mean(axis=0) - ref_prediction.mean(axis=0)


if __name__ == '__main__':
    args = get_options()

    if not os.path.exists(os.path.dirname(args.log)):
        os.mkdir(os.path.dirname(args.log))

    logging.basicConfig(
        filename=args.log,
        filemode='w',
        level=logging.DEBUG,
        format='%(asctime)s %(message)s',
        datefmt='%Y/%d/%m %I:%M:%S %p'
        )  # Note: encoding argument added in python3.9 (here is python3.8)
    
    start = time()

    if not args.score_variants:
        model = Enformer(args.model_url)
        logging.info("Enformer model loaded.")

        target_names = read_targets(args.targets_url)
        logging.info("Target names loaded.")

        logging.info(f"Fasta files path:\n{args.fasta_path}")

        # store and log filters to keep
        if args.filters_keep is None:
            filters_keep_list = []
        elif len(args.filters_keep) > 0:
            try:
                filters_keep_list = open(args.filters_keep[0]).read().splitlines()
            except FileNotFoundError:
                filters_keep_list = args.filters_keep
        else:
            sys.exit('Filters keep option must be a list of filters or a file containing \
                    a list of filters.\nExit code: 1.')
        logging.info("List of columns to keep:")
        for filter in filters_keep_list:
            logging.info(filter)
        
        # store and log filters to drop
        if args.filters_drop is None:
            filters_drop_list = []
        elif len(args.filters_drop) > 0:
            try:
                filters_drop_list = open(args.filters_drop[0]).read().splitlines()
            except FileNotFoundError:
                filters_drop_list = args.filters_drop
        else:
            sys.exit('Filters drop option must be a list of filters or a file containing \
                    a list of filters.\nExit code: 1.')
        logging.info("List of columns to drop:")
        for filter in filters_drop_list:
            logging.info(filter)

        list_fasta_files = get_files(path=args.fasta_path, ext='.fa')
        
        logging.info("List of input fasta files.")
        for file in list_fasta_files:
            logging.info(file)
            file = Path(file)
            df = pred_from_fasta(fasta_file=file, seq_len=args.seq_len)
            df = pd.DataFrame(df, columns=target_names)

            ## make column names unique
            # make a new data frame of column headers and number sequentially
            dfcolumns = pd.DataFrame({'name': df.columns})
            dfcolumns['counter'] = dfcolumns.groupby('name').cumcount().apply(str)

            # remove counter for first case (optional) and combine suffixes
            dfcolumns.loc[dfcolumns.counter=='0', 'counter'] = ''
            df.columns = dfcolumns['name'] + dfcolumns['counter']

            filtered_df = filter_table(df=df, filters_keep_list=filters_keep_list, filters_drop_list=filters_drop_list)
            del df
                
            if args.to_float16:
                filtered_df = filtered_df.astype('float16')

            # change extension to save results table
            if args.to_hdf:
                filtered_df.to_hdf(file.with_suffix('.hdf5'), key="df1")
            else:
                filtered_df.to_csv(file.with_suffix('.csv'), index=False)
            del filtered_df
        
        logging.info(f"Results directories:")
        if args.to_hdf:
            list_tables = [os.path.join(args.fasta_path, path) for path in get_files(args.fasta_path, '.hdf5')]
        else:
            list_tables = [os.path.join(args.fasta_path, path) for path in get_files(args.fasta_path, '.csv')]
        for file in list_tables:
            # get the chromosome directory from fasta path and paste it to results directory to move files in chromosomes directories
            chrom_dir = os.path.dirname(file).split('/')[-1]
            res_tissue_dir = os.path.join(args.results_path, chrom_dir)
            if not os.path.exists(res_tissue_dir):
                os.mkdir(res_tissue_dir)
            logging.info(f"file {file} moved in path {res_tissue_dir}")
            move(file, res_tissue_dir)
    else:
        model = EnformerScoreVariantsRaw(args.model_url)
        logging.info("EnformerScoreVariantsRaw model loaded.")

        target_names = read_targets(args.targets_url)
        logging.info("Target names loaded.")

        logging.info(f"Fasta files path:\n{args.fasta_path}")

        # store and log filters to keep
        if args.filters_keep is None:
            filters_keep_list = []
        elif len(args.filters_keep) > 0:
            try:
                filters_keep_list = open(args.filters_keep[0]).read().splitlines()
            except FileNotFoundError:
                filters_keep_list = args.filters_keep
        else:
            sys.exit('Filters keep option must be a list of filters or a file containing \
                    a list of filters.\nExit code: 1.')
        logging.info("List of columns to keep:")
        for filter in filters_keep_list:
            logging.info(filter)
        
        # store and log filters to drop
        if args.filters_drop is None:
            filters_drop_list = []
        elif len(args.filters_drop) > 0:
            try:
                filters_drop_list = open(args.filters_drop[0]).read().splitlines()
            except FileNotFoundError:
                filters_drop_list = args.filters_drop
        else:
            sys.exit('Filters drop option must be a list of filters or a file containing \
                    a list of filters.\nExit code: 1.')
        logging.info("List of columns to drop:")
        for filter in filters_drop_list:
            logging.info(filter)

        list_path_ref, list_path_alt, root = get_files_vars(path=args.fasta_path, ref=args.ref, alt=args.alt)
        if len(list_path_ref) != len(list_path_alt):
            raise ValueError('Need as much reference fasta as alternative fasta.')

        logging.info("List of input fasta files.")
        for i in range(len(list_path_ref)):
            logging.info(list_path_ref[i])
            logging.info(list_path_alt[i])
            path_ref = Path(list_path_ref[i])
            path_alt = Path(list_path_alt[i])
            df = score_variants(path_ref, path_alt, seq_len=args.seq_len)
            df = pd.DataFrame(df, columns=target_names)
            filtered_df = filter_table(df=df, filters_keep_list=filters_keep_list, filters_drop_list=filters_drop_list)
            # change extension to save results table
            filtered_df.to_csv(f'{root}{args.name_output_scores}.csv', index=False)
            del df
            del filtered_df

        logging.info(f"Results directories:")
        list_csv = [os.path.join(args.fasta_path, path) for path in get_files(args.fasta_path, '.csv')]
        for file in list_csv:
            logging.info(f"file {file} moved in path {args.results_path}")

            path, filename = os.path.split(file)
            try:
                os.remove(os.path.join(args.results_path, filename))
            except FileNotFoundError:
                pass
                
            move(file, args.results_path)

    logging.info(f'Running time: {time()-start}')
