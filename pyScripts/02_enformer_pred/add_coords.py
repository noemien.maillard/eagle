#! /usr/bin/python3
# Creation date: 2024/10/22
# Last review: 2024/10/22
# By Noémien MAILLARD
# Laboratory: GENEPI
# Project: EAGLE
# Script aim: add coordinates to deepsea results.

import argparse
import logging
import natsort
import numpy as np
import pandas as pd
import re
import sys


def get_options():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-f',
        '--prediction-files',
        nargs='+',
      	help='List of prediction files.'
    )
    parser.add_argument(
        '-b',
        '--bed-files',
        nargs='+',
      	help='List of bed files.'
    )
    parser.add_argument(
        '--bed-sep',
        default='\t',
      	help='Separator for bed files. Default to tab.'
    )
    parser.add_argument(
        '-o',
        '--output-path',
        help='Output path. Input files will be save with the same name (but updated path)'
    )
    parser.add_argument(
        '--input-format',
        default='hdf',
        help='Input type format. Accepted values: csv, tsv, hdf and hdf5. Default to hdf. \
                Note that if you choose "csv" option, sep parameter will be the same for input and output.'
    )
    parser.add_argument(
        '--output-format',
        default='hdf',
        help='Output type format. Accepted values: csv, tsv, hdf and hdf5. Default to hdf. \
                Note that if you choose "csv" option, sep parameter will be the same for input and output.'
    )
    parser.add_argument(
	'--log',
	help='Log file name.'
    )
    return parser.parse_args()


def csv2csv(pred_list, bed_list, bed_sep, output_path):
    for pred in pred_list:
        chrom = re.search('chr[a-zA-Z0-9_]+', pred).group()
        bed = [x for x in bed_list if re.search(rf'\b{chrom}\b', x)][0]
        df_pred = pd.read_csv(pred, sep=',')
        df_bed = pd.read_csv(bed, sep=bed_sep)
        pd.concat([df_bed, df_pred], axis=1).to_csv(pred, sep=',', index=False)


def csv2tsv(pred_list, bed_list, bed_sep, output_path):
    for pred in pred_list:
        chrom = re.search('chr[a-zA-Z0-9_]+', pred).group()
        bed = [x for x in bed_list if re.search(rf'\b{chrom}\b', x)][0]
        df_pred = pd.read_csv(pred, sep=',')
        df_bed = pd.read_csv(bed, sep=bed_sep)
        output_path = '/'.join(pred.split('/')[:-1])
        output_file = re.sub('\\..*$', '.tsv', pred.split('/')[-1])
        pd.concat([df_bed, df_pred], axis=1).to_csv('/'.join([output_path, output_file]), sep='\t', index=False)
            

def csv2hdf(pred_list, bed_list, bed_sep, output_path):
    for pred in pred_list:
        chrom = re.search('chr[a-zA-Z0-9_]+', pred).group()
        bed = [x for x in bed_list if re.search(rf'\b{chrom}\b', x)][0]
        df_pred = pd.read_csv(pred, sep=',')
        df_bed = pd.read_csv(bed, sep=bed_sep)
        output_path = '/'.join(pred.split('/')[:-1])
        output_file = re.sub('\\..*$', '.hdf5', pred.split('/')[-1])
        pd.concat([df_bed, df_pred], axis=1).to_hdf('/'.join([output_path, output_file]), key='df1', index=False)


def tsv2csv(pred_list, bed_list, bed_sep, output_path):
    for pred in pred_list:
        chrom = re.search('chr[a-zA-Z0-9_]+', pred).group()
        bed = [x for x in bed_list if re.search(rf'\b{chrom}\b', x)][0]
        df_pred = pd.read_csv(pred, sep='\t')
        df_bed = pd.read_csv(bed, sep=bed_sep)
        output_path = '/'.join(pred.split('/')[:-1])
        output_file = re.sub('\\..*$', '.csv', pred.split('/')[-1])
        pd.concat([df_bed, df_pred], axis=1).to_csv('/'.join([output_path, output_file]), sep=',', index=False)


def tsv2tsv(pred_list, bed_list, bed_sep, output_path):
    for pred in pred_list:
        chrom = re.search('chr[a-zA-Z0-9_]+', pred).group()
        bed = [x for x in bed_list if re.search(rf'\b{chrom}\b', x)][0]
        df_pred = pd.read_csv(pred, sep='\t')
        df_bed = pd.read_csv(bed, sep=bed_sep)
        pd.concat([df_bed, df_pred], axis=1).to_csv(pred, sep='\t', index=False)
            

def tsv2hdf(pred_list, bed_list, bed_sep, output_path):
    for pred in pred_list:
        chrom = re.search('chr[a-zA-Z0-9_]+', pred).group()
        bed = [x for x in bed_list if re.search(rf'\b{chrom}\b', x)][0]
        df_pred = pd.read_csv(pred, sep='\t')
        df_bed = pd.read_csv(bed, sep=bed_sep)
        output_path = '/'.join(pred.split('/')[:-1])
        output_file = re.sub('\\..*$', '.hdf5', pred.split('/')[-1])
        pd.concat([df_bed, df_pred], axis=1).to_hdf('/'.join([output_path, output_file]), key='df1', index=False)


def hdf2csv(pred_list, bed_list, bed_sep, output_path):
    for pred in pred_list:
        chrom = re.search('chr[a-zA-Z0-9_]+', pred).group()
        bed = [x for x in bed_list if re.search(rf'\b{chrom}\b', x)][0]
        df_pred = pd.read_hdf(pred, key='df1')
        df_bed = pd.read_csv(bed, sep=bed_sep)
        output_path = '/'.join(pred.split('/')[:-1])
        output_file = re.sub('\\..*$', '.csv', pred.split('/')[-1])
        pd.concat([df_bed, df_pred], axis=1).to_csv('/'.join([output_path, output_file]), sep=',', index=False)


def hdf2tsv(pred_list, bed_list, bed_sep, output_path):
    for pred in pred_list:
        chrom = re.search('chr[a-zA-Z0-9_]+', pred).group()
        bed = [x for x in bed_list if re.search(rf'\b{chrom}\b', x)][0]
        df_pred = pd.read_hdf(pred, key='df1')
        df_bed = pd.read_csv(bed, sep=bed_sep)
        output_path = '/'.join(pred.split('/')[:-1])
        output_file = re.sub('\\..*$', '.tsv', pred.split('/')[-1])
        pd.concat([df_bed, df_pred], axis=1).to_csv('/'.join([output_path, output_file]), sep='\t', index=False)

            
def hdf2hdf(pred_list, bed_list, bed_sep, output_path):
    for pred in pred_list:
        chrom = re.search('chr[a-zA-Z0-9_]+', pred).group()
        bed = [x for x in bed_list if re.search(rf'\b{chrom}\b', x)][0]
        df_pred = pd.read_hdf(pred, key='df1')
        df_bed = pd.read_csv(bed, sep=bed_sep)
        pd.concat([df_bed, df_pred], axis=1).to_hdf(pred, sep='\t', index=False)


def main(pred_list, bed_list, bed_sep, output_path, input_format, output_format): 
    # input = csv
    if input_format == 'csv' and output_format == 'csv':
        csv2csv(pred_list, bed_list, bed_sep, output_path)
    elif input_format == 'csv' and output_format == 'tsv':
        csv2tsv(pred_list, bed_list, bed_sep, output_path)
    elif input_format == 'csv' and output_format in ['hdf', 'hfd5']:
        csv2hdf(pred_list, bed_list, bed_sep, output_path)

    # input = tsv
    if input_format == 'tsv' and output_format == 'csv':
        csv2csv(pred_list, bed_list, bed_sep, output_path)
    elif input_format == 'tsv' and output_format == 'tsv':
        csv2tsv(pred_list, bed_list, bed_sep, output_path)
    elif input_format == 'tsv' and output_format in ['hdf', 'hfd5']:
        csv2hdf(pred_list, bed_list, bed_sep, output_path)

    # input = hdf5
    if input_format in ['hdf', 'hdf5'] and output_format == 'csv':
        hdf2csv(pred_list, bed_list, bed_sep, output_path)
    elif input_format in ['hdf', 'hdf5'] and output_format == 'tsv':
        hdf2tsv(pred_list, bed_list, bed_sep, output_path)
    elif input_format in ['hdf', 'hdf5'] and output_format in ['hdf', 'hfd5']:
        hdf2hdf(pred_list, bed_list, bed_sep, output_path)


if __name__ == '__main__':
    args = get_options()

    logging.basicConfig(
	filename=args.log,
	filemode='w',
	level=logging.DEBUG,
	format='%(asctime)s %(message)s',
	datefmt='%Y/%d/%m %I:%M:%S %p'
    )

    logging.info(f'Input prediction files: {args.prediction_files}')
    logging.info(f'Input bed files: {args.bed_files}')
    logging.info(f'Input bed separator: {args.bed_sep}')
    logging.info(f'Output path: {args.output_path}')
    logging.info(f'Input format: {args.input_format}')
    logging.info(f'Output format: {args.output_format}')

    if args.input_format not in ['csv', 'tsv', 'hdf', 'hdf5']:
        raise ValueError('Accepted input formats: csv, tsv, hdf, hdf5')

    if args.output_format not in ['csv', 'tsv', 'hdf', 'hdf5']:
        raise ValueError('Accepted output format: csv, tsv, hdf, hdf5.')

    if len(args.prediction_files) != len(args.bed_files):
        raise ValueError('The number of prediction_files and bed_files must be the same.')

    main(
        pred_list=args.prediction_files,
        bed_list=args.bed_files,
        bed_sep=args.bed_sep,
	output_path=args.output_path,
        input_format=args.input_format,
        output_format=args.output_format
    )
