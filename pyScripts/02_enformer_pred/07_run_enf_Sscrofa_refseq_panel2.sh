#!/usr/bin/bash

# fat (liver and adipose)
python3 01_run_enformer.py --fasta-path /media/nomaillard/Storage/EAGLE/data/enformer_genome_binned/enformer_Sus_scrofa_genome_binned/ \
	--filters-keep /media/nomaillard/Storage/EAGLE/results/enformer/Sscrofa/BSgenome/filter_files/fat.txt \
	--filters-drop fetal embryo crispr fibrobl endothelial cell_line origin derived \
	--results-path /media/nomaillard/Storage/EAGLE/results/enformer/Sscrofa/Sscrofa11.1/fat/ \
	--log /media/nomaillard/Storage/EAGLE/results/enformer/Sscrofa/Sscrofa11.1/fat/fat.log
# muscle
python3 01_run_enformer.py --fasta-path /media/nomaillard/Storage/EAGLE/data/enformer_genome_binned/enformer_Sus_scrofa_genome_binned/ \
	--filters-keep /media/nomaillard/Storage/EAGLE/results/enformer/Sscrofa/BSgenome/filter_files/muscle.txt \
	--filters-drop fetal embryo crispr fibrobl endothelial cell_line origin derived \
	--results-path /media/nomaillard/Storage/EAGLE/results/enformer/Sscrofa/Sscrofa11.1/muscle/ \
	--log /media/nomaillard/Storage/EAGLE/results/enformer/Sscrofa/Sscrofa11.1/muscle/muscle.log
# lung
python3 01_run_enformer.py --fasta-path /media/nomaillard/Storage/EAGLE/data/enformer_genome_binned/enformer_Sus_scrofa_genome_binned/ \
	--filters-keep lung \
	--filters-drop fetal embryo crispr fibrobl endothelial cell_line origin derived \
	--results-path /media/nomaillard/Storage/EAGLE/results/enformer/Sscrofa/Sscrofa11.1/lung/ \
	--log /media/nomaillard/Storage/EAGLE/results/enformer/Sscrofa/Sscrofa11.1/lung/lung.log
# spleen
python3 01_run_enformer.py --fasta-path /media/nomaillard/Storage/EAGLE/data/enformer_genome_binned/enformer_Sus_scrofa_genome_binned/ \
	--filters-keep spleen \
	--filters-drop fetal embryo crispr fibrobl endothelial cell_line origin derived \
	--results-path /media/nomaillard/Storage/EAGLE/results/enformer/Sscrofa/Sscrofa11.1/spleen/ \
	--log /media/nomaillard/Storage/EAGLE/results/enformer/Sscrofa/Sscrofa11.1/spleen/spleen.log
