#! /usr/bin/python3
# Creation date: 2023/11/17
# Last review: 2024/10/10
# By Noémien MAILLARD
# Laboratory: GENEPI
# Project: EAGLE
# Script aim: Concatenate enformer prediction files.

import argparse
import os
from natsort import natsorted
import pandas as pd


def get_options():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-f',
        '--files',
        nargs='+',
        help='List of files to concatenate.'
    )
    parser.add_argument(
        '-o',
        '--output',
        help='Output file name.'
    )
    parser.add_argument(
        '--sep',
        default='\t',
        help='Input and output tables separator. Default to tab.'
    )
    parser.add_argument(
	'--to-hdf',
	action="store_true",
	help='If true, save to hdf5 format. Key will be "df1". Default to False.'
    )
    parser.add_argument(
	'--rm-files',
	action='store_true',
	help='If true, remove files concatenated (after saving concat table). Default to false.'
    )
    return parser.parse_args()


def sort_and_concat(files_list, sep):
    # sort files in numerical order
    sorted_list = natsorted(files_list)
    # read files in list ready to concatenate
    if sorted_list[0][-3:] == 'csv':
        df_list = [pd.read_csv(file, sep=sep) for file in sorted_list]
    elif sorted_list[0][-4:] == 'hdf5':
        df_list = [pd.read_hdf(file, key="df1") for file in sorted_list]

    return pd.concat(df_list)


if __name__ == '__main__':
    args = get_options()
    if not os.path.exists(os.path.dirname(args.output)):
        os.mkdir(os.path.dirname(args.output))
    sort_and_concat(files_list=args.files, sep=args.sep).to_hdf(args.output, key="df1")
    if args.rm_files:
        for file in args.files:
            os.remove(file)
