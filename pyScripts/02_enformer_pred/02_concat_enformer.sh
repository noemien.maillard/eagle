#!/usr/bin/bash

## lung and spleen predictions
chr_dirs='/media/nomaillard/Storage/EAGLE/results/enformer/Sscrofa_genome_binned/lung_spleen/'
output_path='/media/nomaillard/Storage/EAGLE/results/enformer/Sscrofa_genome_binned/lung_spleen/concat_chrom/'

for scaffold in `ls $chr_dirs`;
do
	if [[ -f $chr_dirs$scaffold ]] || [[ $scaffold == "concat_chrom" ]] 
	then
		continue
	fi
	echo "concatenating files for $scaffold..."
	# scores_files are files containing predictions
	scores_files=`ls $chr_dirs$scaffold/*.csv`
	python ../01_deepbind_pred_Sscrofa/concat_deepbind.py --files $scores_files --output $output_path"$scaffold.enformer_windows_393216b.csv" --sep ","
done
