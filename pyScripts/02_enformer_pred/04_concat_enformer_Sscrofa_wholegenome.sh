#!/usr/bin/bash

tissues=(blood brain fat glands muscle pancreas repro)

for tissue in ${tissues[@]};
do
	echo $tissue
	## setup paths given tissues
	chr_dirs='/media/nomaillard/Storage/EAGLE/results/enformer/Sscrofa_genome_binned/'$tissue'/'
	output_path='/media/nomaillard/Storage/EAGLE/results/enformer/Sscrofa_genome_binned/'$tissue'/concat_chrom/'

	for scaffold in `ls $chr_dirs`;
	do
		# ignore files (log) and concat_chrom directory
		if [[ -f $chr_dirs$scaffold ]] || [[ $scaffold == "concat_chrom" ]] 
		then
			continue
		fi
		echo "concatenating files for $scaffold..."
		# scores_files are files containing predictions
		scores_files=`ls $chr_dirs$scaffold/*.csv`
		python3 ../02_concat_df.py --files $scores_files --output $output_path"$scaffold.enformer_windows_393216b.csv" --sep ","
	done
done
