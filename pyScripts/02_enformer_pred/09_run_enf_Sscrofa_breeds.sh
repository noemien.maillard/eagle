#!/usr/bin/bash

breeds=(bamei berkshire hampshire jinhua landrace largewhite meishan pietrain rongchang tibetan usmarc wuzhishan)

for breed in ${breeds[@]}
do
	results_dir="/media/nomaillard/Storage/EAGLE/results/enformer/Sscrofa/Sscrofa_"$breed"/01_predictions"
	mkdir -p $results_dir
	# fat (liver and adipose)
	python3 01_run_enformer.py --fasta-path "/media/nomaillard/Storage/EAGLE/data/enformer_genome_binned/enformer_Sus_scrofa_"$breed"_genome_binned/" \
		--filters-keep /media/nomaillard/Storage/EAGLE/results/enformer/Sscrofa/BSgenome/filter_files/fat.txt \
		--filters-drop fetal embryo crispr fibrobl endothelial cell_line origin derived \
		--results-path $results_dir/fat/ \
		--log $results_dir/fat/fat.log
	# muscle
	python3 01_run_enformer.py --fasta-path /media/nomaillard/Storage/EAGLE/data/enformer_genome_binned/enformer_Sus_scrofa_genome_binned/ \
		--filters-keep /media/nomaillard/Storage/EAGLE/results/enformer/Sscrofa/BSgenome/filter_files/muscle.txt \
		--filters-drop fetal embryo crispr fibrobl endothelial cell_line origin derived \
		--results-path $results_dir/muscle/ \
		--log $results_dir/muscle/muscle.log
	# lung
	python3 01_run_enformer.py --fasta-path /media/nomaillard/Storage/EAGLE/data/enformer_genome_binned/enformer_Sus_scrofa_genome_binned/ \
		--filters-keep lung \
		--filters-drop fetal embryo crispr fibrobl endothelial cell_line origin derived \
		--results-path $results_dir/lung/ \
		--log $results_dir/lung/lung.log
	# spleen
	python3 01_run_enformer.py --fasta-path /media/nomaillard/Storage/EAGLE/data/enformer_genome_binned/enformer_Sus_scrofa_genome_binned/ \
		--filters-keep spleen \
		--filters-drop fetal embryo crispr fibrobl endothelial cell_line origin derived \
		--results-path $results_dir/spleen/ \
		--log $results_dir/spleen/spleen.log
done
