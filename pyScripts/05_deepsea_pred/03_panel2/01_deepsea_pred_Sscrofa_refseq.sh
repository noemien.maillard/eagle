#!/usr/bin/bash

intervals_files=/media/nomaillard/Storage/EAGLE/data/deepsea_genome_binned/deepsea_Sus_scrofa_genome_binned/intervals.bins
deepsea=/home/nomaillard/Documents/Projects/EAGLE/pyScripts/05_deepsea_pred/01_run_deepsea_gpu.py
for i in {1..10}
do
	python3 $deepsea --intervals-file $intervals_files$i.bed \
		--fasta-file /media/nomaillard/Storage/EAGLE/data/ref_seq/Sscrofa/Sscrofa_allbreeds_release112/Sus_scrofa.Sscrofa11.1.dna.toplevel_chrrenamed.fa \
		--names-file /home/nomaillard/Documents/deepsea/kipoi/predictor_names.txt \
		--keep-cols ctcf h3k4me1 h3k4me3 h3k27ac h3k27me3 dnase \
		--drop-cols fetal smooth osteoblast fibroblast lymphoblast trophoblast derived \
		--output /media/nomaillard/Storage/EAGLE/results/deepsea/Sscrofa/Sscrofa11.1/01_predictions/sus_scrofa_$i \
		--log /media/nomaillard/Storage/EAGLE/results/deepsea/Sscrofa/Sscrofa11.1/01_predictions/sus_scrofa_$i.log
done
