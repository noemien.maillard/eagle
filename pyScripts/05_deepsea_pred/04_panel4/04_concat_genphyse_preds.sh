#!/usr/bin/bash


concat=/home/nomaillard/Documents/Projects/EAGLE/pyScripts/02_concat_df.py
breeds=(Sscrofa_AB63083986_LW_ref Sscrofa_AB75005564_LWm3 Sscrofa_AB75005811_DU Sscrofa_AB75069227_PE810 Sscrofa_AB75069229_LRm6 Sscrofa_AB75132735_LW Sscrofa_AB75167259_PI Sscrofa_AB75230671_LR Sscrofa_AB75230687_TZM)
results=/media/nomaillard/Storage/EAGLE/results/deepsea/Sscrofa/

for breed in ${breeds[@]}
do
	python $concat --files `find $results$breed'/01_predictions' -type f -name '*tsv'` \
		--output $results$breed'01_predictions/concat_chroms.tsv'
done
