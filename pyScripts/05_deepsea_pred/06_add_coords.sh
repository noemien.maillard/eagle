#!/usr/bin/bash


filtered_df_path=/media/nomaillard/Storage/EAGLE/results/deepsea/Sscrofa/
bed_path=/media/nomaillard/Storage/EAGLE/data/deepsea_genome_binned/
#ori_df_path=/work/genphyse/genepi/noemien/01_predictions/deepsea/Sscrofa/
breeds=(AB63083986_LW_ref AB75005564_LWm3 AB75005811_DU AB75069227_PE810 AB75069229_LRm6 AB75132735_LW AB75167259_PI AB75230671_LR AB75230687_TZM)

for breed in ${breeds[@]}
do
	python3 add_coords.py --prediction-files `find $filtered_df_path'Sscrofa_'$breed'/01_predictions/' -name *tsv` \
		--bed-files `find $bed_path'deepsea_Sus_scrofa_'$breed'_genome_binned/' -name *bed` \
		--output-path $filtered_df_path'Sscrofa_'$breed'/01_predictions/'  \
		--input-format tsv \
		--output-format tsv \
		--log $filtered_df_path'Sscrofa_'$breed'/01_predictions/add_coords.log'
done
