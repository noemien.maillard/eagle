# Creation date: 2024/09/13
# Last review: 2024/09/13
# By Noémien MAILLARD
# Laboratory: GENEPI
# Project: EAGLE
# Script aim: Read deepSEA predictions and save it into 1 file per chromosome.


import logging
import natsort
from os import remove
import pandas as pd


def get_options():
    import argparse

    parser = argparse.ArgumentParser()

    parser.add_argument(
        '--hdf-files',
        nargs='+',
        help="List of raw prediction files to sort and read."
    )
    parser.add_argument(
        '--bed-files',
        nargs='+',
        help="List of bed files to add 'chr', 'start' and 'end' to output files."
    )
    parser.add_argument(
        '--bed-sep',
        default='\t',
        help="Separator to read bed files. Default to tab."
    )
    parser.add_argument(
        '--output-path',
        help="Path to save files."
    )
    parser.add_argument(
        '--prefix',
        help='Prefix for output files. No default (chromosome names in output).'
    )
    parser.add_argument(
        '--rm-hdf',
        action='store_true',
        help="If true, remove the initial input hdf5 files (duplicates). Default to false."
    )
    parser.add_argument(
            '--rm-bed',
            action='store_true',
            help="If true, remove the bed files. Default to false."
    )
    parser.add_argument(
        '--log',
        help="Log file name. Default to {output-path}pred2chr.log"
    )

    return parser.parse_args()


def main():
    args = get_options()

    output = args.output_path if str(args.output_path).endswith('/') else f'{args.output_path}/'
    log = args.log if args.log is not None else f'{output}pred2chr.log'
    logging.basicConfig(
            filename=log,
            filemode='w',
            level=logging.DEBUG,
            format='%(asctime)s %(message)s',
            datefmt='%Y/%d/%m %I:%M:%S %p'
            ) 

    list_keys = [f'intervals{i+1}' for i in range(len(args.hdf_files))]
    logging.info(f'list_keys (ordered): {list_keys}')
    hdf = pd.concat([pd.read_hdf(file, key) for file, key in zip(natsort.natsorted(args.hdf_files), list_keys)])
    logging.info('hdf loaded.')
    bed = pd.concat([pd.read_csv(file, sep=args.bed_sep, names=['chr', 'start', 'end']) for file in args.bed_files])
    logging.info('bed loaded.')
    hdf = pd.concat([bed, hdf], axis=1)
    logging.info('bed and hdf concatenated.')
    del(bed)

    chr_names = hdf['chr'].unique()
    hdf = [y for _, y in hdf.groupby('chr')]
    if args.prefix is None:
        for chr_df, name in zip(hdf, chr_names):
            chr_df.to_hdf(f'{output}{name}.hdf5', key='df1')
            logging.info(f'{name} saved')
    else:
        for chr_df, name in zip(hdf, chr_names):
            chr_df.to_hdf(f'{output}{args.prefix}{name}.hdf5', key='df1')
            logging.info(f'{name} saved')
    
    logging.info(f'rm_hdf: {args.rm_hdf}')
    if args.rm_hdf:
        for file in args.hdf_files:
            remove(file)

    logging.info(f'rm_bed: {args.rm_bed}')
    if args.rm_bed:
        for file in args.bed_files:
            remove(file)


if __name__ == '__main__':
    main()
